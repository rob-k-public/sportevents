import exception.EventNotFoundException;
import service.EventService;
import service.EventServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, EventNotFoundException, IOException {
        EventService service = new EventServiceImpl();
        String eventId = "";

        while (!eventId.equals("0")) {
            System.out.print("Please enter the event number: ");
            eventId = new BufferedReader(new InputStreamReader(System.in)).readLine();
            try {
                if (!eventId.equals("0")) {
                    System.out.println(service.getEvent(eventId).toString());
                }
            } catch (EventNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("END OF SEASON");
    }
}
