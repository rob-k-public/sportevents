package dao;

import entity.Event;
import entity.Participant;
import exception.EventNotFoundException;

import java.util.List;

public interface EventRepository {

    Event getEventInformation(String id) throws EventNotFoundException;
    List<Participant> getParticipants(Event event);
}
