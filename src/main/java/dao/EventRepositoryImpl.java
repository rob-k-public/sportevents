package dao;

import entity.Event;
import entity.Participant;
import exception.EventNotFoundException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventRepositoryImpl implements EventRepository {

    private static final String HOST = "jdbc:mysql://localhost:3306/athletedb";
    private static final String USERNAME = ""; //FILL IN YOUR USERNAME
    private static final String PASSWORD = ""; //FILL IN YOUR PASSWORD
    private Connection connection;

    public EventRepositoryImpl() throws SQLException {
        connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
    }

    public Event getEventInformation(String id) throws EventNotFoundException {
        String query = "SELECT * FROM event WHERE eventID = " + id;
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if (!rs.next()) {
                throw new EventNotFoundException("No event found for id: " + id);
            }
            return new Event(rs.getInt(1), rs.getString(2), rs.getString(4), rs.getString(3));
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        throw new EventNotFoundException("No event found for id: " + id);
    }

    public List<Participant> getParticipants(Event event) {
        String query = "select a.Fname, a.Lname, a.Sex, pi.Performance_in_minutes from event\n" +
                "INNER JOIN participation_ind pi ON event.eventID = pi.eventID\n" +
                "INNER JOIN athlete a ON  a.athleteID = pi.athleteID\n" +
                "WHERE event.eventID = " + event.getId() +"\n" +
                "ORDER BY Performance_in_minutes ASC";
        List participants = new ArrayList<Participant>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Participant participant = new Participant(rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(3));
                participants.add(participant);
            }
            return participants;
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return Collections.emptyList();
    }
}
