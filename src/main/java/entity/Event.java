package entity;

public class Event {

    private int id;
    private String name;
    private String date;
    private String location;

    public Event(int id, String name, String date, String location) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "The running event " + name + " is organized on " + date + " at " + location;
    }

    public String getName() {
        return name;
    }
}
