package entity;

import java.util.List;

public class EventWithParticipants {

    private Event event;
    private List<Participant> participants;
    private int numberOfParticipants;

    public EventWithParticipants(Event event, List<Participant> participants) {
        this.event = event;
        this.participants = participants;
        numberOfParticipants = participants.size();
    }

    private String generateLineOfCharacters(int amount, String character){
        String line = "";
        for (int i = 0; i < amount; i++){
            line += character;
        }
        return line;
    }

    private String printParticipants(){
        String out = "";
        for (Participant participant : participants){
            out += participant.toString() + "\n";
        }
        return out;
    }

    @Override
    public String toString(){
        String out  = "";
        out += event.toString() + "\n";
        out += generateLineOfCharacters(20, "*") + "\n";
        out += event.getName() + " has " + numberOfParticipants + " participants." + "\n";
        out += "\n";
        out += "First name // Last name // Performance" + "\n";
        out += generateLineOfCharacters(10, "-") + "\n";
        out += printParticipants() + "\n";
        out += "The winner of " + event.getName() + " is " + getWinner("M") + " (men) and " + getWinner("F") + " (women).";

        return out;
    }

    private String getWinner(String sex) {
        for (Participant participant : participants){
           if (participant.getSex().equals(sex) && !participant.getPerformance().equals("***")){
               return participant.getFullName();
           }
        }
        return "***";
    }
}
