package entity;

public class Participant {

    private String firstName;
    private String lastName;
    private String performance;
    private String sex;

    public Participant(String firstName, String lastName, String performance, String sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.performance = performance;
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public String getPerformance(){
        return performance == null ? "***" : performance + " minutes";
    }

    @Override
    public String toString() {
        return firstName + " // " + lastName + " // " + getPerformance();
    }
}
