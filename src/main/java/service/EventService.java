package service;

import entity.EventWithParticipants;
import exception.EventNotFoundException;

public interface EventService {

    EventWithParticipants getEvent(String id) throws EventNotFoundException;
}
