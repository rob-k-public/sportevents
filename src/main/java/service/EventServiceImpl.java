package service;

import dao.EventRepository;
import dao.EventRepositoryImpl;
import entity.Event;
import entity.EventWithParticipants;
import entity.Participant;
import exception.EventNotFoundException;

import java.sql.SQLException;
import java.util.List;

public class EventServiceImpl implements EventService {

    private EventRepository repository;

    public EventServiceImpl() throws SQLException {
        repository = new EventRepositoryImpl();
    }

    public EventWithParticipants getEvent(String id) throws EventNotFoundException {
        Event event = repository.getEventInformation(id);
        List<Participant> participants = repository.getParticipants(event);
        return new EventWithParticipants(event, participants);
    }
}
